#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>
#define my_number_processor MPI_COMM_WORLD

int cont(int a, int b, int i, int n, int count)
{   
    while (i < n)
    {
        a = rand();
        b = rand();
        if ((a * a) + (b * b) < 1)
        {
            count++;
            printf("Solucion %f\n", count);
        }
        i++;
    }
    return (4 * ((double)count / n));
}

void main()
{
    int n, count, i, a, b, c, rank, size;
    float resp, output;
    a = 0;
    b = 0;
    i = 0;
    c = 0;
    count = 0;
    MPI_Init(NULL, NULL);
    MPI_Comm_size(my_number_processor, &size);
    MPI_Comm_rank(my_number_processor, &rank);
    MPI_Bcast(&n, 1, MPI_INT, 0, my_number_processor);
      if (rank == 0)
    {
        printf("Ingrese un valor\n");
        scanf("%d", &n);
    }
    if (n <= 0)
    {
        MPI_Finalize();
        exit(0);
    }
    else
    {
        resp = cont(a, b, i, n, c);
        if (rank == 0)
        {
            printf("Solucion %f\n", resp);
        }
    }
    MPI_Finalize();
}
